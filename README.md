Ansible Role: Keycloak
=========

Installs and configures keycloak server

Requirements
------------

No special requirements

Role Variables
--------------
[defaults/main.yml](defaults/main.yml)

Dependencies
------------

None

Example Playbook
----------------
    - name: Setup keycloak
      hosts: keycloak
      become: true
      gather_facts: true
      roles:
        - ansible-keycloak

Inside `vars/main.yml`

*Minimal installation without db in dev mode*

    keycloak_admin_user: "keycloak"
    keycloak_admin_password: "changeme"

*Signle host nstallation with postgresql and self signed certificate in production mode*

    keycloak_use_pgsql: true
    keycloak_admin_user: "keycloak"
    keycloak_admin_password: "changeme"
    keycloak_db_name: "keycloak_db"
    keycloak_db_username: "keycloak"
    keycloak_db_password: "changeme"
    keycloak_db_ip: "{{ hostvars['db01.ru-central1.internal']['ansible_facts']['default_ipv4']['address'] }}"
    keycloak_start_option: "start"
    keycloak_create_self_signed_cert: true
    keycloak_ansible_validate_certs: false
    keycloak_auth_url: "https://{{ ansible_facts['fqdn'] }}:8443"
    keycloak_config_options:
      - option: "db"
        value: "postgres"
      - option: "db-url"
        value: "jdbc:postgresql://{{ keycloak_db_ip }}/{{ keycloak_db_name }}"
      - option: "db-username"
        value: "{{ keycloak_db_username }}"
      - option: "db-password"
        value: "{{ keycloak_db_password }}"
      - option: "hostname"
        value: "{{ ansible_facts['fqdn'] }}"
      - option: "https-certificate-file"
        value: "{{ keycloak_pem_dest }}"
      - option: "https-certificate-key-file"
        value: "{{ keycloak_key_dest }}"

*Cluster installation with postgresql and self signed certificate in production mode using JDBC_PING*

    keycloak_use_pgsql: true
    keycloak_admin_user: "keycloak"
    keycloak_admin_password: "changeme"
    keycloak_db_name: "keycloak_db"
    keycloak_db_username: "keycloak"
    keycloak_db_password: "changeme"
    keycloak_db_ip: "{{ hostvars['db01.ru-central1.internal']['ansible_facts']['default_ipv4']['address'] }}"
    keycloak_start_option: "start"
    keycloak_create_self_signed_cert: true
    keycloak_ansible_validate_certs: false
    keycloak_auth_url: "https://{{ ansible_facts['fqdn'] }}:8443"
    keycloak_setup_jdbc_ping_config: true
    keycloak_envs:
      KEYCLOAK_ADMIN: "{{ keycloak_admin_user }}"
      KEYCLOAK_ADMIN_PASSWORD: "{{ keycloak_admin_password }}"
      KC_CACHE_CONFIG_FILE: "cache-ispn-jdbc-ping.xml"
      KC_DB: "postgres"
      KC_DB_URL_HOST: "{{ hostvars['db01.ru-central1.internal']['ansible_facts']['default_ipv4']['address'] }}"
      KC_DB_URL_DATABASE: "{{ keycloak_db_name }}"
      KC_DB_USERNAME: "{{ keycloak_db_username }}"
      KC_DB_PASSWORD: "{{ keycloak_db_password }}"
      KC_LOG_LEVEL: "INFO,org.infinispan:DEBUG,org.jgroups:DEBUG"
      JGROUPS_DISCOVERY_EXTERNAL_IP: "{{ ansible_facts['default_ipv4']['address'] }}"
    keycloak_config_options:
      - option: "db"
        value: "postgres"
      - option: "db-url"
        value: "jdbc:postgresql://{{ keycloak_db_ip }}/{{ keycloak_db_name }}"
      - option: "db-username"
        value: "{{ keycloak_db_username }}"
      - option: "db-password"
        value: "{{ keycloak_db_password }}"
      - option: "hostname"
        value: "{{ ansible_facts['fqdn'] }}"
      - option: "https-certificate-file"
        value: "{{ keycloak_pem_dest }}"
      - option: "https-certificate-key-file"
        value: "{{ keycloak_key_dest }}"

License
-------

MIT

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
